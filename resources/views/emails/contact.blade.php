<div class="container">
    <h3>Üzenete érkezett a nagykanizsai-adomanybolt.hu oldalról!</h3>
    <div class="table-responsive">
      <table class="table">
          <tbody>
              <tr>
                <td>Név:</td>
                <td>{{ $data['name'] }}</td>
              </tr>
              <tr>
                <td>E-mail:</td>
                <td>{{ $data['email'] }}</td>
              </tr>
              <tr>
                <td>Üzenet</td>
              </tr>
              <tr>
                <td>{{ $data['message'] }}</td>
              </tr>
          </tbody>
      </table>
    </div>
</div>
