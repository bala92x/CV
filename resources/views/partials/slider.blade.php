<!-- SECTION FULLSCREEN -->
<section id="section1" class="parallax halfscreen" style="background: url('images/laptop-min.jpg'); background-size: cover;">

    <div class="container text-light text-right">
        <div class="container-fullscreen">
            <div class="
	            @if ($agent->isMobile())
	                text-left
	            @else
	                text-middle
	            @endif
            ">
                <div class="
                    @if ($agent->isMobile())
                        text-uppercase text-large
                    @else
                        heading-hero
                    @endif
                    "

                data-animation="fadeInDown" data-animation-delay="300">BUDAVÖLGYI BÁLINT</div>
                <p id="mypos" class="lead" data-animation="fadeInDown" data-animation-delay="600"><strong><em>Junior web-programozó
                    @if ($agent->isMobile())
                        <br>
                    @else
                        -
                    @endif
                Önéletrajz</em></strong></p>
            </div>
        </div>
    </div>


</section>
<!-- end: SECTION FULLSCREEN -->
