<!-- Header -->
<header id="header" class="header-transparent dark">
    <div id="header-wrap">
        <div class="container">
            <!--Logo-->
            <div id="logo">
                <a href="/" class="logo" data-dark-logo="/images/me.png">
                    <img src="/images/me.png" alt="Budavölgyi Bálint">
                </a>
            </div>
            <!--End: Logo-->

            <!--Navigation Resposnive Trigger-->
            <div id="mainMenu-trigger">
                <button class="lines-button x"> <span class="lines"></span> </button>
            </div>
            <!--end: Navigation Resposnive Trigger-->

            <!--Navigation-->
            <div id="mainMenu" class="light">
                <div class="container">
                    <nav>
                        <ul>
                            <li><a href="/">06/20 398-1231</li></a></li>
                            <li><a href="/">Kezdőlap</a></li>
                            <li><a href="/motivacios-level">Motivációs <i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!--end: Navigation-->
        </div>
    </div>
</header>
<!-- end: Header --> 