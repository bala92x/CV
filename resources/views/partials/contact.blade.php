<!-- GET IN TOUCH -->
<section id="section6" class="background-grey">
	<div class="container">

		<div class="text-center m-b-100">
			<h1 class="text-medium">Mindezek után szeretne alkalmazni?</h1>
			<p>Teljesen megértem Önt! Kérem írja meg gratulációját és a kezdésem várható időpontját!</p>
		</div>
		<div class="row">
			<div class="col-md-8 center">
				<form class="widget-contact-form" action="/sendmail" role="form" method="post">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Név</label>
                            <input type="text" aria-required="true" name="name" class="form-control required name" placeholder="Adja meg a nevét">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" aria-required="true" name="email" class="form-control required email" placeholder="Adja meg az email címét">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message">Üzenet</label>
                        <textarea type="text" name="message" rows="9" class="form-control required" placeholder="Gratuláció és várható kezdés"></textarea>
                    </div>
	                <div class="row">
                        <div class="form-group text-center">
	                    	<button class="btn btn-default center" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Felveszem!</button>
	                    </div>
	                </div>
                </form>
                    
			</div>
		</div>
	</div>
</section>
<!-- end: GET IN TOUCH -->