<!-- EDUCATION -->
<section id="section3" class="background-overlay-gradient-dark" style="background-image: url('images/bg-min.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    ">

    <div class="container text-light">

        <div class="heading">
            <h2>
                @if ($agent->isMobile())
                    TANULMÁNYOK
                @else
                    TANULMÁNYAIM
                @endif
            </h2>
        </div>
        <div class="row">
            <div class="col-md-3
                @if ($agent->isMobile())
                    border-bottom
                @else
                    border-right
                @endif
            ">
                <h3 class="edu">Középiskola</h3>
                <h4 class="m-b-0 edu">Dr. Mező Ferenc Gimnázium</h4>
                <p>2006 - 2010</p>
                <p>Legtöbb német órán meghajtogatott papírhajó díj 2009</p>
            </div>
            <div class="col-md-3
                @if ($agent->isMobile())
                    border-bottom
                @else
                    border-right
                @endif
            ">
                <h3 class="edu">Angol középfokú nyelvvizsga</h3>
                <h4 class="m-b-0 edu">BME</h4>
                <p>2013</p>
                <p>Old MacDonald had a farm, I-E-I-O.</p>
            </div>
            <div class="col-md-3
                @if ($agent->isMobile())
                    border-bottom
                @else
                    border-right
                @endif
            ">
                <h3 class="edu">Sportedzői OKJ</h3>
                <h4 class="edu m-b-0">Testnevelési Főiskola</h4>
                <p>2013 - 2014</p>
                <p>Online ment az egész tanfolyam, csak vizsgázni mentem.</p>
            </div>
            <div class="col-md-3
                @if ($agent->isMobile())
                    border-bottom
                @else
                    border-right
                @endif
            ">
                <h3 class="edu">Rekreációszervezés BSc</h3>
                <h4 class="m-b-0 edu">Nyugat- Magyarországi Egyetem</h4>
                <p>2010 - 2013</p>
                <p>3 év értetlenül állás, hogy miért kell az úszásórát szerda reggel 7 órára, 2 órával a keddi buli után tenni.</p>
            </div>
        </div>
    </div>
</section>
<!-- end: EDUCATION -->
