<section id="section5" class="background-grey">
	<div class="container">
		<div class="text-center p-b-60">
    		<h1>Miért is vagyok én a legalkalmasabb a munkakörre?</h1>
    	</div>
	    <div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #1abc9c"><strong>A cég kétségtelenül jól jár velem hiszen:</strong></div>
				<div class="panel-body" id="panel-left">
					<ul class="list-icon list-icon-arrow list-icon-colored">
			    		@if ($agent->isMobile())

            			@else
                			<br><br>
            			@endif
            			<li>Bizonyítottan 8 óránál is tovább vagyok képes dolgozni, a facebook fiókom csekkolása nélkül</li>
			    		<li>Autentikus ördögi kacajom van. (Opcionális)</li>
			    		<li>Van egy zavarbaejtően profi (félkész) weboldalam, amit most nem mutatok meg, mert akkor nem lesz miről beszélgetni az interjún.</li>
			    		@if ($agent->isMobile())

                        @else
                            <li>Ismerek minden HTML varázsigét pl.: <br>A<br>akada<br>a</li>
                        @endif
			    		<li>Nem fogom senki elől elenni a rántott húst. (Lásd. gyengeségeim)</li>
			    		<li>Rugalmas vagyok a munkaidő beosztás terén, hiszen minden nap 8-tól 4-ig ráérek!</li>
			    		<li>Akkor is tudok nevetni a főnök poénján, amikor nem vicces.</li>
			    	</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #1abc9c"><strong>És akkor beszéljünk az anyagiakról</strong></div>
				<div class="panel-body">
					<p>Természetesen szeretnék sok pénzt keresni. A nettó bérigényem havi <code>11111101111010000</code> - <code>100111000100000000</code> Ft a próbaidő/junior program lejárta után. Cafeteria nem követelmény, de előny. Amennyiben alkalmaznak, igényt tartok céges kerékpárra/rollerre a munkába járáshoz.</p>
					<blockquote>Az emberek 95%-a meglepődik, ha a mondatok nem úgy végződnek, ahogyan ők azt krumplifőzelék.</blockquote>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #1abc9c"><strong>Mégegyszer kérem, vegyen fel!</strong></div>
				<div class="panel-body">
					<p>Tényleg hagyna engem feledésbe merülni ezen a mai könyörtelen és brutális munkaerőpiacon? Most komolyan, hol élünk? Állítólag már szív- és szájsebészeti asszisztensre sincs szükség Sepsiszentgyörgyön!</p>
				</div>
			</div>
		</div>
	</div>

</section>
<section class="background-overlay-gradient-dark" style="background-image: url('images/bg-min.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    ">
    <div class="container text-light">
        <!--Default pie chart-->
        <div class="hr-title hr-long center"><abbr style="color: #1abc9c">Alkalmasság

        	@if ($agent->isMobile())

            @else
                a pozícióra
            @endif
        </abbr> </div>
        <div class="row">
            <div class="col-md-3 text-center"> <span class="pie-chart" data-color="#EA4C89" data-percent="100"> <span class="percent"></span> </span>
                <h4>ÉN</h4>
            </div>
            <div class="col-md-3 text-center">
                <div class="pie-chart" data-percent="93" data-color="#FF675B"> <span class="percent"></span> </div>
                <h4>BILL GATES</h4>
            </div>
            <div class="col-md-3 text-center">
                <div class="pie-chart" data-percent="81" data-color="#1abc9c"> <span class="percent"></span> </div>
                <h4>A FŐNÖK HAVERJA</h4>
            </div>
            <div class="col-md-3 text-center">
                <div class="pie-chart" data-percent="66" data-color="#FF9900"> <span class="percent"></span> </div>
                <h4>AZ ELŐZŐ JELENTKEZŐ</h4>
            </div>
        </div>
        <!--END: Default pie chart-->
    </div>
</section>
