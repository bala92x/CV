<section id="section3" class="background-grey">
    <div class="container">

        <div class="container text-center">
            <h2>CSALÁDI FOTÓK</h2>
            <h5 class="lead p-b-30">

            @if ($agent->isMobile())
                Felül:
            @else
                Bal oldalon:
            @endif
             Megboldogult dédnagyapám ölében a Balcsin.
            <br> 

            @if ($agent->isMobile())
                Alul:
            @else
                Jobb oldalon:
            @endif
             A dédi kezdetlegesnek mondható téziseit boncolgatjuk.</h5>
        </div>
        <div class="col-md-4 parallax halfscreen" style="background: url('images/einstein2.jpg'); background-size: cover;">
        </div>
        <div id="einstein" class="col-md-8 parallax halfscreen" style="background: url('/images/einstein.jpg');">
        </div>
    </div>
</section>