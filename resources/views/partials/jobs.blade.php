<!--Horizontal tabs border -->
<section id="section4" class="background-grey">
	<div class="container">
		<div id="tabs-05" class="tabs border">
			<h2 class="heading heading text-left">TAPASZTALATOK</h3>
			<p>Néhány korábbi, korántsem releváns munkám</p>
			<ul class="tabs-navigation">
				<li role="tabpanel" class="tab-pane fade in active" id="szallito"><a href="#Szallito"><i class="fa fa-car"></i>Szállító</a> </li>
				<li role="tabpanel" class="tab-pane fade" id="kulcsfigura"><a href="#Kulcsfigura"><i class="fa fa-key"></i>Kulcsfigura</a> </li>
				<li role="tabpanel" class="tab-pane fade" id="supervisor" ><a href="#Supervisor"><i class="fa fa-thumbs-o-up"></i>Supervisor</a> </li>
				<li role="tabpanel" class="tab-pane fade" id="teammember"><a href="#TeamMember"><i class="fa fa-users"></i>Team Member</a> </li>
			</ul>
			<div class="tabs-content">
				<div class="tab-pane active" id="Szallito">
					<h4>Nagykanizsa, 2016. máj - jelenleg</h4>
					<p>A szállító magányos ember. A profik mindig magányosak. Egy erődszerű házban él, ha hívják, megjelenik szuper járgányán, hibátlanul, időre házhoz szállítja, amit rábíznak, majd elporzik. Sosem kérdez, sosem akar többet tudni, mint amennyit muszáj, sosem hibázik. Szigorúan betartja saját szabályait.</p>
				</div>
				<div class="tab-pane" id="Kulcsfigura">
					<h4>Zalakaros, 2015. máj - 2016. máj</h4>
					<p>Én voltam az a pancser, akire rá lehetett bízni mindent a szökőkút pucolástól a szaunaszeánszig. Eljöttem, mert a minimálbérből nem futotta az aranyér kenőcsömre.</p>
				</div>
				<div class="tab-pane" id="Supervisor">
					<h4>Aberdeen, Skócia, 2014. ápr - 2015. máj</h4>
					<p>A helyi sportkomplexumban voltam pultban állós, colgate mosolyú, lengyel akctentussal beszélő kolléga, 2 beosztottal. Amikor nem voltak sokan, pasziánszoztam. </p>
				</div>
				<div class="tab-pane" id="TeamMember">
					<h4>Aberdeen, Skócia, 2014. feb - 2014. ápr</h4>
					<p>Egy dinamikusan fejlődő, nemzetközi piacon is dominánsan jelen lévő cégnél volt szerencsém eltanulni a precizitás, gyors munkavégzés és kifogástalan megfigyelőképesség mesterfogásait. Amúgy Burger King. </p>
				</div>
			</div>
		</div>
	</div>	
</section>
<!--END: Horizontal tabs border -->