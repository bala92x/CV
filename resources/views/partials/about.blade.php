<!--ABOUT -->
<section id="section2" class="background-grey">
    <div class="container">
        <div class="row  m-b-50">
            <div class="col-md-3">
                <div class="heading heading text-left">
                    <h2 id="aboutme">RÓLAM</h2>

                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6"><p>Budavölgyi Bálint vagyok, 25 éves nagykanizsai junior web-programozó. Körülbelül fél éve szerettem bele a programozásba, azóta kódolva kelek és fekszem. Szerencsére ezalatt az idő alatt sikerült szereznem 37 év releváns munkatapasztalatot, és megtanultam tömérdek hasznos, és izgalmas tényt. Tudta Ön például, hogy ha az önéletrajz tartalmaz legalább egy családi fotót, az 100%-kal növeli a jelentkező esélyét az állás megszerzésére? (Ígérem, hogy ennek eleget teszek az oldal közepe felé!)</p>
                        <br>
                        <br><p>Kedvenc időtöltéseim közé tartozik a szóló tandem kerékpározás és a húrelméleten való morfondírozás mellett, a zenélés. Játszom többek között gitáron, mandolinon és tenor banjon, mindemellett páratlan énekhanggal rendelkezem, melyről <a target="_blank" class="rickroll" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">videóbizonyítékom</a> is van</p>

                @if ($agent->isMobile())
                    </div>
                @endif
                </div>
                <div class="row">  
                    <div class="col-md-6"><p>Ha mindez még mindig nem lenne elég, széleskörű online marketing és SEO ismeretekkel rendelkezem. Hadd dörgöljek az orra alá néhány kifejezést a sok közül, melyeket betéve tudok:</p> 
                    <ul class="list-icon list-icon-arrow list-icon-colored text-center">
                        <li>CTR</li>
                        <li>Backlink</li>
                        <li>Affiliate Marketing</li>
                        <li>Bounce Rate</li>
                        <li>CFA</li>
                    </ul>
                        <br>
                        <br>
                        <div class="col-md-12 p-t-10">
                            @if ($agent->isMobile())
                                <br>
                            @endif
                            <blockquote>
                                <h6>"Vegyenek már fel, könyörgöm!"</h6>
                                <small class="text-right"><cite>Ősi hajótörött bölcsesség</cite></small>
                            </blockquote>
                        </div>


                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="heading heading text-left m-b-20">
                    <h2>SZAKMAI KÉSZSÉGEIM</h2>

                </div>A következő, fegyvertáramból kiemelt kis ízelítő hivatott bizonyítani szakmai rátermettségemet.
            </div>
            <div class="col-md-9">
                <div class="m-t-60">
                    <div class="progress-bar-container title-up small extra-small color">
                        <div class="progress-bar" data-percent="100" data-delay="100" data-type="%">
                            <div class="progress-title">OBJECT ORIENTÁLT MÉLÁZÁS</div>
                        </div>
                    </div>

                    <div class="progress-bar-container title-up small extra-small color">
                        <div class="progress-bar" data-percent="94" data-delay="200" data-type="%">
                            <div class="progress-title">SZÁMOLÓGÉP KEZELÉS</div>
                        </div>
                    </div>

                    <div class="progress-bar-container title-up small extra-small color">
                        <div class="progress-bar" data-percent="78" data-delay="300" data-type="%">
                            <div class="progress-title">IT VICCEK</div>
                        </div>
                    </div>

                    <div class="progress-bar-container title-up small extra-small color">
                        <div class="progress-bar" data-percent="65" data-delay="400" data-type="%">
                            <div class="progress-title">KÁVÉFŐZÉS</div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</section>
<!--END: ABOUT -->