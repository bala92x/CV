<!-- WHAT WE DO -->
<section id="section4" class="background-overlay-gradient-dark" style="background-image: url('images/bg-min.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    ">
	<div class="container text-light">
		<div class="heading text-left">
			<h2>
				@if ($agent->isMobile())
                	ERŐSSÉGEK
            	@else
                	ERŐSSÉGEIM
            	@endif
			</h2>
			<span class="lead">Néhány legfontosabb erényem a sok közül.</span>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="0">
					<h4>Kimeríthetetlen ötletarzenál</h4>
					<p>Átlag félpercenként pattannak ki a fejemből korszakalkotó ötletek, melyeket azonnal leírok. Előfordult, hogy betűlevesből kellett programot írnom.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="200">
					<h4>Virológia védettség</h4>
					<p>Szerencsére voltam már bárányhimlős, így a kollegáknak nem kell tartaniuk, hogy tőlem kapják majd el.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="400">
					<h4>Billentyűkombinációk ismerete</h4>
					<p>Széles körben ismerem és alkalmazom őket. Kedvenc, és leggyakrabban használt kombinációim a következők: <kbd>Ctrl+C</kbd>  -  <kbd>Ctrl+V</kbd> ( <kbd>Ctrl+CV?</kbd> )</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="600">
					<h4>Fényes jövőkép</h4>
					<p>Nem csak azt ígérhetem meg, hogy az Önök cégétől fogok nyugdíjállományba vonulni, hanem azt is, hogy szülési szabadságra sem megyek egy jó pár évig biztosan.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="800">
					<h4>Páratlan végtagi differenciáltság</h4>
					<p>Egy időben tudom a fejemet simogatni, és a másik kezemmel paskolni a hasamat.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="1000">
					<h4>Hibátlan hejesírás</h4>
					<p>6 éves korom, ota alszom a magyar hejesírás szótáral a felyem alatt.</p>
				</div>
			</div>


		</div>
	</div>
</section>
<!-- END WHAT WE DO -->
