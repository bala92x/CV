<!-- WHAT WE DO -->
<section class="background-overlay-gradient-dark" style="background-image: url('images/bg-min.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    ">
	<div class="container text-light">
		<div class="heading text-left">
			<h2>
				@if ($agent->isMobile())
                	GYENGESÉGEK
            	@else
                	GYENGESÉGEIM
            	@endif
			</h2>
			<span class="lead">Természetesen nincsenek, azonban a <code>&ltform&gt</code>aságok kedvéért párat kitaláltam.</span>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="0">
					<h4>Gyenge tüdő</h4>
					<p>Bármennyit is edzek, nem tudom a lélegzetemet 10 percnél tovább visszatartani.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="200">
					<h4>Kötött karízületek</h4>
					<p>Valószínűtlennek tartom, hogy valaha is képes legyek megnyalni a könyökömet.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="400">
					<h4>Borzalmas kézügyesség</h4>
					<p>Ez tulajdonképpen előny is, mivel biztosan nem fogok két hónap után lelépni Ausztriába pincérnek.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="600">
					<h4>Csak részegen beszélek csehül</h4>
					<p>Viszont akkor folyékonyan! Valószínűleg azért, mert olyankor csehül érzem magamat. Chci pracovat a dobrodružství nahoru!</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="800">
					<h4>Félek a fogorvostól</h4>
					<p>No &nbsp&nbsp<code>/*&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp*/</code></p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="" data-animation="fadeInUp" data-animation-delay="1000">
					<h4>Válogatós vagyok</h4>
					<p>Nem eszem meg a rántotthúst, sőt a betűleves sincsen a kedvencek listáján, mert sokáig tart utána a takarítás.</p>
				</div>
			</div>


		</div>
	</div>
</section>
<!-- END WHAT WE DO -->
