<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('components.head')
</head>
<body id="wrapper" class="boxed body-bg" style="
	
    animation-duration: 1500ms; 
    opacity: 1;
">
	<!--Tracking code me-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-103857206-1', 'auto');
	  ga('send', 'pageview');

	</script>

	<!--Tracking code Schulcz-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-102178388-1', 'auto');
	  ga('send', 'pageview');

	</script>
        
    @include('components.navbar')
    @include('validation.errors')     
    @yield('content')

    @include('components.footer')
    @include('components.scripts')
</body>
</html>