@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable fade in alert-message-own">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('success'))
    <div class="alert alert-success alert-dismissable fade in alert-message-own">
        {{ \Session::get('success') }}
    </div>
@endif

@if (\Session::has('error'))
    <div class="alert alert-danger  alert-dismissable fade in alert-message-own">
        {{ \Session::get('error') }}
    </div>
@endif