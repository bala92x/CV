<!-- Footer -->
<footer id="footer" class="footer-light copyright-content">
        <div class="copyright-text text-center">
            &copy; 2017 BUDAVÖLGYI BÁLINT - MINDEN JOG FENNTARTVA AZ ÁLLÁSRA<br>HA NEM ÉRTETTE A YOUTUBE LINKES POÉNT&nbsp;  
            <button class="btn btn-xs btn-slide" href="https://hu.wikipedia.org/wiki/Rickrolling">
                <i class="fa fa-arrow-right"></i>
                <span><a target="_blank" href="https://hu.wikipedia.org/wiki/Rickrolling">WIKIPÉDIA</a></span>
            </button>
        </div>
</footer>
<!-- end: Footer -->
@if ($agent->isMobile())

@else
    <!-- Go to top button -->
	<a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
@endif
