<!-- Header -->
<header id="header" class="header-transparent dark">
    <div id="header-wrap">
        <div class="container">
            <!--Logo-->
            <div id="logo">
                <a href="/" class="logo" data-dark-logo="/images/me.png">
                    <img src="/images/me.png" alt="Budavölgyi Bálint">
                </a>
            </div>
            <!--End: Logo-->

            <!--Navigation Resposnive Trigger-->
            <div id="mainMenu-trigger">
                <button class="lines-button x"> <span class="lines"></span> </button>
            </div>
            <!--end: Navigation Resposnive Trigger-->

            <!--Navigation-->
            <div id="mainMenu" class="light">
                <div class="container">
                    {{-- <nav class="pull-left"> --}}
                        {{-- <ul> --}}
                            {{-- <li><a class="scroll-to" style="color: #252525;" href="#section6">06/20 398-1231</li></a></li> --}}
                        {{-- </ul> --}}
                    {{-- </nav> --}}
                    <nav>
                        <ul>
                            <li><a class="scroll-to" href="#section2">Rólam</a></li>
                            <li><a class="scroll-to" href="#section3">Tanulmányok</a></li>
                            <li><a class="scroll-to" href="#section4">Tapasztalat</a></li>
                            <li><a class="scroll-to" href="#section5">Motivációs <i class="fa fa-envelope"></i></a></li>
                            <li><a class="scroll-to" href="#section6">Kapcsolat</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!--end: Navigation-->
        </div>
    </div>
</header>
<!-- end: Header --> 