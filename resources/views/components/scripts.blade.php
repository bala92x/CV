<!--Plugins-->
<script src="/js/jquery.js"></script>
<script src="/js/plugins.js"></script>

<!--Template functions-->
<script src="/js/functions.js"></script>

<!--Toggleable tabs-->
<script type="text/javascript">
	$('#teammember').tab('show')
	$('#supervisor').tab('show')
	$('#kulcsfigura').tab('show')
	$('#szallito').tab('show')
</script>
