<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszónak legalább hat karakter hosszúnak kell lennie, és egyeznie kell a megerősítéssel!',
    'reset' => 'Sikeresen lecserélte a jelszavát!',
    'sent' => 'Elküldtük a jelszó változtatáshoz szükséges linket!',
    'token' => 'A jelszó emlékeztető helytelen.',
    'user' => "Nem található felhasználó ezzel az email címmel.",

];
