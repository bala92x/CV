<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
     public function contactMail(Request $request)
    {
    	$validator = \Validator::make($request->all(), [
    		'name' => 'required',
    		'email' => 'required|email',
    		'message' => 'required',
    	]);
		
		if ($validator->fails())
			return back()->withErrors($validator)->withInput();

		$data = $request->all();

		\Mail::send('emails.contact', ['data' => $data], function ($m) use ($data) {
			$m->from(env('FROM_EMAIL'));
			$m->to(env('ADMIN_EMAIL'))->subject('Felvéve!');
		});

		return \Response::json(
			[
				'response' => 'success',
			]
		);

	}
}
